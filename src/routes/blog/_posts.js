// Ordinarily, you'd generate this data from markdown files in your
// repo, or fetch them from a database of some kind. But in order to
// avoid unnecessary dependencies in the starter template, and in the
// service of obviousness, we're just going to leave it here.

// This file is called `_posts.js` rather than `posts.js`, because
// we don't want to create an `/blog/posts` route — the leading
// underscore tells Sapper not to do that.

const posts = [
	{
		title: 'More About Intelliwatt',
		slug: 'more-about-intelliwatt',
		html: `
			<p>As electricity grid development has expanded, electrical engineers and other professionals have been thinking up ways to economically and functionally stretch the power supply to communities by giving electricity consumers more options and power companies more control. The IoT Device Communication Aggregator is novel in that power consumers and power companies will both be satisfied with the product. Changes in power routines will likely go unnoticed for users and they will save money on their power bills. Meanwhile, power companies will have the freedom to distribute power where it’s needed during peak times which, in effect, will eventually eliminate the constant upkeep of standby power plants which are cost exorbitant.</p>

			<p>The delivered system will consist of a server that bridges the gap between electric companies and end-users. For instance, if the utility (electric company) notices that demand is going up at any given time, then the utility can send a message to the server to limit electricity to homes that the server oversees. The server will send out a request to each home it is over to limit electricity by x number of kilowatt/hours for y amount of time. Each participating home will have a specialized piece of hardware (called an aggregator) that will then use its algorithm to limit electricity to some appliances in the home. The user will be able to hand off control to the aggregator via delivered software so that only the devices that aren’t needed at a certain time can be limited. For example, the user may hand off control for charging an electric vehicle at night. The aggregator would then be able to use its algorithm to limit electrical usage at certain times (determined by the utility) to obtain optimum electricity savings while still providing a charged vehicle in the morning when it’s time to go to work.</p>	
		`
	},
	{
		title: 'User Tutorial',
		slug: 'user-tutorial',
		html: `
			
		`
	},
	{
		title: 'Meet the Developers',
		slug: 'meet-the-developers',
		html: `
			<div style="display:flex; padding-bottom: 35px;">
				<div style="padding-right: 30px;">
					<h2>Yiju</h2>
					<img alt="yiju" src="yijuPic.jpg" />
				</div>
				<div style="padding-right: 30px;">
					<small>Yiju is a senior computer science student at the University of Utah and expects to graduate in Dec. 2020. Two 						years ago, Yiju began his computer science degree journey and achieved various fascinating projects. In Spring 							2019, Yiju accomplished with a team of five to apply the Angle development process to Google 		spreadsheet-like program with a client written in C# and server written in C++. Then, in Mar. 2020, Yiju started his capstone project 	with a team of three, who is responsible for setting up fundamental features, such as authentication, database query, and overall 	front-end UI structure. During the summer break in 2019, Yiju also sought a hardware manufacturing company for his software development internship, which he helped the company design an application to control micro medical devices. After graduation, Yiju is most likely pursuing full-stack development careers. Furthermore, Yiju likes to hike, travel, and invest stocks during his spare time, as well as helping his friends with their coding assignments occasionally.
					</small>
				</div>
			</div>
			<div style="display:flex; padding-bottom: 35px;">
				<div style="padding-right: 30px;">
					<h2>Jacob</h2>
					<img alt="jacob" src="jacobPic.png" />
				</div>
				<div style="padding-right: 30px;">
					<small>
						Jacob is a University of Utah student majoring in Computer Science. He was born and raised in Utah in the Great Salt Lake valley. Religion is an important aspect of Jacob's life, and he is a member of the Church of Jesus Christ of Latter-day Saints. After graduating from Brighton High School in 2013 he served a two year religious mission in various cities in Oregon. When he got back home he immedatiely started school at the University of Utah in the Computer Science program. Additionally, he got his first job working for America First Credit Union as a loan officer. He has been working hard ever since to get his bachelor's degree. When Jacob is not studying he likes to run, hike, and play the piano. He is a home body who enjoys seeing his family often. He plans to write quality code for a big company sometime soon after he graduates in December of 2020. For this project he was in charge of the back end server code(written in c++) as well as the home user network code(written in Java).
					</small>
				</div>
			</div>
			<div style="display:flex;">
				<div style="padding-right: 30px;">
					<h2>Gemma</h2>
					<img alt="gemma" src="gemmaPic.jpg" />
				</div>
					<div style="padding-right: 30px;">
					<small> Gemma never expected to study computer science, but caught the coding bug a few years ago. Her contribution to the Intelliwatt project was creating the UI for the utility web page. She's been involved in a few research projects at the University of Utah, one of them being the task of making touchscreen games for monkeys to study their brains. She's spent the last two years serving and expanding the Women in Computing group at the U and has also loved working as a teaching assistant for CS intro classes for the last 3 years. She interned at a local company Summer of 2020 and will be returning as a full-time junior software engineer upon graduation. In her free time, she likes to cook, knit sweaters, and travel. Her family is from Italy, and she met her Finnish husband while working in Sweden 9 years ago, so visiting family in Europe is a favorite activity when she can find the time.
					</small>
				</div>
			</div>
		`
	},

	{
		title: 'Our Tech Stack',
		slug: 'our-tech-stack',
		html: `
			<div style="display:flex;">
				<div style="padding-right: 30px;">
					<p>Task Manager</p>
					<img alt="gitlab" src="gitlab.jpg" />
				</div>
				<div style="padding-right: 30px;">
					<p>Versioning System</p>
					<img alt="git" src="git.png" />
				</div>
				<div>
					<p>Hosting Service</p>
					<img alt="vercel" src="vercel.png" />
				</div>
			</div>

			<div style="display:flex;">
				<div style="padding-right: 30px;">
					<p>Front End</p>
					<img alt="next" src="next.jpg" />
				</div>
				<div style="padding-right: 30px;">
					<p>Back End</p>
					<img alt="java" src="java.png" />
				</div>
				<div>
					<p>Database</p>
					<img alt="mongo" src="mongo.jpg" />
				</div>
			</div>

			<div style="display:flex;">
				<div style="padding-right: 30px;">
					<p>Data Distribution Service</p>
					<img alt="rti" src="rtiPic.png" />
				</div>
				<div>
					<p>Platforms Supported: Web</p>
				</div>
			</div>
		`
	},

	{
		title: 'How Can I Learn More?',
		slug: 'how-can-i-learn-more',
		html: `
			<p>This is an ongoing project created and supported by the <a href='https://usmart.ece.utah.edu/'>Utah Smart Energy Lab</a>.</p>
		`
	}
];

posts.forEach(post => {
	post.html = post.html.replace(/^\t{3}/gm, '');
});

export default posts;
